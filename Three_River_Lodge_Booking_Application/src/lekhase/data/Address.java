/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.data;

/**
 *
 * @author JM Lekhase
 */
public class Address {
   // Declaring Variables
    private String street;
    private String city;
    private int postCode;

    //default constructor 
    public Address() {
        this("", "", 0);
    }

    // Overlaod constructor 
    public Address(String street, String city, int postCode) {
        setStreet(street);
        setCity(city);
        setPostCode(postCode);
    }

    //Validating Setters
    public void setStreet(String street) {
        if (street.length() > 3) {
            this.street = street;
        } else {
            throw new IllegalArgumentException("Please enter more than 3 Characters");
        }

    }

    public void setCity(String city) {

        if (city.length() > 3) {
            this.city = city;
        } else {
            throw new IllegalArgumentException("Please enter more than 3 Characters");
        }

    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    //Getters
    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public int getPostCode() {
        return postCode;
    }

    //displaying Address info
    public String displayAddress() {
        return street + " , " + city + " , " + postCode;
    }

    //Display
    @Override
    public String toString() {
        return "--------Address--------------\n"
                + "Street: " + street + "\n"
                + "City: " + city + "\n"
                + "PostCode: " + postCode;
    }

}
