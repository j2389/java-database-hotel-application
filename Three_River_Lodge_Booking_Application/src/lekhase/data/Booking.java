/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.data;

import java.util.ArrayList;


/**
 *
 * @author JM Lekhase
 */
public class Booking {

     // Declaring Variables
    public enum RoomType {

        S, D, G
    };

    private String roomRef;
    private int daysStayed, NoOfPeople;
    private double amountPay;
    private Client client;
    private RoomType roomType;

    //default constructor
    public Booking() {
        this(" ", 0, 0, 0.0, null, null);
    }

     // Overlaod constructor
    public Booking(String roomRef, int daysStayed, int NoOfPeople, double amountPay, Client client, RoomType roomType) {
        setRoomRef(roomRef);
        setDaysStayed(daysStayed);
        setNoOfPeople(NoOfPeople);
        setAmountPay(amountPay);
        setClient(client);
        setRoomType(roomType);
    }

    //Getters
    public String getRoomRef() {
        return roomRef;
    }

    public int getDaysStayed() {
        return daysStayed;
    }

    public int getNoOfPeople() {
        return NoOfPeople;
    }

    public double getAmountPay() {
        return amountPay;
    }

    public Client getClient() {
        return client;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    //Setters
    public void setRoomRef(String roomRef) {
        this.roomRef = roomRef;
    }

    public void setDaysStayed(int daysStayed) {
        this.daysStayed = daysStayed;
    }

    public void setNoOfPeople(int NoOfPeople) {
        this.NoOfPeople = NoOfPeople;
    }

    public void setAmountPay(double amountPay) {
        this.amountPay = amountPay;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    //Calculating The Amout Pay
    public double calcAmountPayable() {
        final double DISCOUNT_FIFTEEN = 0.15;
        final double DISCOUNT_TEN = 0.1;
        final double DISCOUNT_SEVENANTYFIVE = 0.75;

        if (getRoomType()== RoomType.S) {
            if (getDaysStayed() >= 14) {
                amountPay = (1250 * NoOfPeople * daysStayed) - ((1250 * NoOfPeople * daysStayed) * DISCOUNT_FIFTEEN);
            } else if (getDaysStayed() >= 10 && getDaysStayed()< 14) {
                amountPay = (1250 * NoOfPeople * daysStayed) - ((1250 * NoOfPeople * daysStayed) * DISCOUNT_TEN);
            } else if (getDaysStayed() >= 7 && getDaysStayed() < 10) {
                amountPay = (1250 * NoOfPeople * daysStayed) - ((1250 * NoOfPeople * daysStayed) * DISCOUNT_SEVENANTYFIVE);
            } else {
                amountPay = 1250 * NoOfPeople * daysStayed;
            }

        } else if (getRoomType() == RoomType.D) {
            if (getDaysStayed() >= 14) {
                amountPay = (1380 * NoOfPeople * daysStayed) - ((1380 * NoOfPeople * daysStayed) * DISCOUNT_FIFTEEN);
            } else if (getDaysStayed() >= 10 && getDaysStayed() < 14) {
                amountPay = (1380 * NoOfPeople * daysStayed) - ((1380 * NoOfPeople * daysStayed) * DISCOUNT_TEN);
            } else if (getDaysStayed() >= 7 && getDaysStayed() < 10) {
                amountPay = (1380 * NoOfPeople * daysStayed) - ((1380 * NoOfPeople * daysStayed) * DISCOUNT_SEVENANTYFIVE);
                amountPay = 1380 * NoOfPeople * daysStayed;
            }
        } else {
            if (getDaysStayed() >= 14) {
                amountPay = (1495 * NoOfPeople * daysStayed) - ((1495 * NoOfPeople * daysStayed) * 0.15);
            } else if (getDaysStayed() >= 10 && getDaysStayed() < 14) {
                amountPay = (1495 * NoOfPeople * daysStayed) - ((1495 * NoOfPeople * daysStayed) * 0.10);
            } else if (getDaysStayed() >= 7 && getDaysStayed() < 10) {
                amountPay = (1495 * NoOfPeople * daysStayed) - ((1495 * NoOfPeople * daysStayed) * 0.075);
            } else {
                amountPay = 1495 * NoOfPeople * daysStayed;
            }
        }

        return amountPay;
    }
//Display
    public String displayBookingDetails() {

        return "Room Reference No: " + roomRef + "\n"
                + client.displayclientDetails() + " \n"
                + "Room Code: " + roomType + "\n"
                + "Days Stayed: " + daysStayed + "\n"
                + "Amount Payable: R " + amountPay;
    }

    //Display
    @Override
    public String toString() {
        return  "\n"
                +"----------Booking Details---------- \n"
                + "Room Reference No: " + roomRef + "\n"
                + "Room Code: " + roomType + "\n"
                + "Days Stayed: " + daysStayed + "\n"
                + "Number Of People: " + NoOfPeople + "\n"
                + "Amount Payable: R " + amountPay + "\n"
                + client.displayclientDetails() + "\n"
                +"====================================";

    }
 // Database Method
    public void initConnection() throws DataStorageException {
        BookingDA.initConnection();

    }

    public void terminate() throws DataStorageException {
        BookingDA.terminate();
    }

    public Integer countBooking(String roomType) throws NotFoundException {
        return BookingDA.countBooking(roomType);
    }

    public Double BookingTotalAmount() throws NotFoundException {
        return BookingDA.BookingTotalAmount();
    }

    public Booking returnBooking(String roomRef) throws NotFoundException {
        return BookingDA.returnBooking(roomRef);
    }
   
    public ArrayList<Booking> getAllBookings() throws NotFoundException {
        return BookingDA.getAllBookings();
    }

    public void AddBooking(Address objAddres, Client objClient, Booking objBook) throws NotFoundException {
        BookingDA.AddBooking(objAddres, objClient, objBook);
    }

    public ArrayList<Booking> getLargestBooking() throws NotFoundException {
        return BookingDA.getLargestBooking();
    }

    public ArrayList<String> getRoomRefno() throws NotFoundException {
        return BookingDA.getRoomRefno();
    }

}
