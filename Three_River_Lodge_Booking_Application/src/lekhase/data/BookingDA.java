/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JM Lekhase
 */
public class BookingDA {

    private static Connection conn;
    private static PreparedStatement ps;
    private static ResultSet rs;
    private static final ArrayList<Booking> ArBooking = new ArrayList<>();
    private static final ArrayList<String> ArString = new ArrayList<>();
    

    public static void initConnection() throws DataStorageException {
        String username = "root";
        String password = "";
        String url = "jdbc:mysql://localhost:3306/ThreeRiverLodgeBookingDB";
        String regDriver = "com.mysql.cj.jdbc.Driver";

        try {
            Class.forName(regDriver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new DataStorageException("Connection Failed " + e.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new DataStorageException("Failed to load driver " + ex.getMessage());
        }
    }//Connecting database

    public static void terminate() throws DataStorageException {
        try {
            if (conn != null) {
                conn.close();
            }

        } catch (SQLException e) {
            throw new DataStorageException("Termination Successful " + e.getMessage());
        }
    }//Termination

    public static Integer countBooking(String roomType) throws NotFoundException {
       int num = 0;
        try {
            ps = conn.prepareStatement("SELECT COUNT(*) AS total FROM tblThreeRiverLodgeBookings WHERE RoomType= ? ");
            ps.setString(1, roomType);
            rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt("total");

            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        return num;
    }//Count Bookings Made 

    public static Double BookingTotalAmount() throws NotFoundException {
        double totalPrice = 0;
        try {
            ps = conn.prepareStatement("select Sum(TotalPrice) From  tblThreeRiverLodgeBookings");

            rs = ps.executeQuery();
            while (rs.next()) {
                totalPrice =rs.getDouble("Sum(TotalPrice)");
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        return totalPrice;
    }//Total Amounts

    public static Booking returnBooking(String roomRef) throws NotFoundException {
        Booking objBook = null;
        Address objAddres = null;
        Client objClient = null;
        try {
            ps = conn.prepareStatement("select * From  tblThreeRiverLodgeBookings WHERE RoomRef = ?");
            ps.setString(1, roomRef);
            rs = ps.executeQuery();
            while (rs.next()) {
                objAddres = new Address(rs.getString("street"), rs.getString("city"), rs.getInt("PostalCode"));
                objClient = new Client(rs.getString("idNumber"), rs.getString("title"), rs.getString("ClientName"), objAddres);
                objBook = new Booking(rs.getString("roomRef"), rs.getInt("NoOfDays"), rs.getInt("NoOfPeople"), rs.getDouble("TotalPrice"), objClient, Booking.RoomType.valueOf(rs.getString("roomType")));
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        return objBook;
    }//Returning Customer Information
    
    

    public static ArrayList<Booking> getAllBookings() throws NotFoundException {
        ArBooking.clear();
        Address objAddres = null;
        Client objClient = null;

        try {
            ps = conn.prepareStatement("select * from  tblThreeRiverLodgeBookings");
            rs = ps.executeQuery();

            while (rs.next()) {

               objAddres = new Address(rs.getString("street"), rs.getString("city"), rs.getInt("PostalCode"));
                objClient = new Client(rs.getString("idNumber"), rs.getString("title"), rs.getString("ClientName"), objAddres);
                ArBooking.add(new Booking(rs.getString("roomRef"), rs.getInt("NoOfDays"), rs.getInt("NoOfPeople"), rs.getDouble("TotalPrice"), objClient, Booking.RoomType.valueOf(rs.getString("roomType"))));
            }

        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }

        return ArBooking;
    }// Getting all Booking

    public static void AddBooking(Address objAddres, Client objClient, Booking objBook) throws NotFoundException {
        try {
            ps = conn.prepareStatement("insert into tblThreeRiverLodgeBookings"
                    +"(RoomRef,NoOfDays,NoOfPeople,RoomType,Title,ClientName,IDNumber,Street,City,PostalCode,TotalPrice)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, objBook.getRoomRef());
            ps.setInt(2, objBook.getDaysStayed());
            ps.setInt(3, objBook.getNoOfPeople());
            ps.setString(4, String.valueOf(objBook.getRoomType()));
            ps.setString(5, objClient.getTitle());
            ps.setString(6, objClient.getName());
            ps.setString(7, objClient.getIdNumber());
            ps.setString(8, objAddres.getStreet());
            ps.setString(9, objAddres.getCity());
            ps.setInt(10, objAddres.getPostCode());
            ps.setDouble(11, objBook.calcAmountPayable());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
    }//Add Customer Information to a Database

    public static ArrayList<Booking> getLargestBooking() throws NotFoundException {
        ArBooking.clear();
        Address objAddres = null;
        Client objClient = null;
        try {
            ps = conn.prepareStatement("SELECT * FROM  tblThreeRiverLodgeBookings WHERE TotalPrice =(SELECT MAX(TotalPrice) FROM tblThreeRiverLodgeBookings)");

            rs = ps.executeQuery();
            while (rs.next()) {
               objAddres = new Address(rs.getString("street"), rs.getString("city"), rs.getInt("PostalCode"));
                objClient = new Client(rs.getString("idNumber"), rs.getString("title"), rs.getString("ClientName"), objAddres);
                ArBooking.add(new Booking(rs.getString("roomRef"), rs.getInt("NoOfDays"), rs.getInt("NoOfPeople"), rs.getDouble("TotalPrice"), objClient, Booking.RoomType.valueOf(rs.getString("roomType"))));
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        return ArBooking;
    }//Getting Largest Booking
    
     public static  ArrayList<String>getRoomRefno() throws NotFoundException{
        ArString.clear();
        try {
            ps = conn.prepareStatement("select RoomRef from tblThreeRiverLodgeBookings WHERE RoomType = 'S' OR  RoomType = 'D' OR  RoomType = 'G'");
            rs = ps.executeQuery();
            
            while (rs.next()) {   
                ArString.add(rs.getString("RoomRef"));
                
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        
        return ArString;
    }//Get Room Reference number

}
