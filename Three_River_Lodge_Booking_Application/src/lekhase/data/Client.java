/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.data;

/**
 *
 * @author JM Lekhase
 */
public class Client {

     // Declaring Variables
    private String idNumber;
    private String title, name;
    private Address address;

    //default constructor
    public Client() {
        this("", "", "", null);
    }

    // Overlaod constructor
    public Client(String idNumber, String title, String name, Address address) {
        setIdNumber(idNumber);
        setTitle(title);
        setName(name);
        setAddress(address);
    }

    //Getters
    public String getIdNumber() {
        return idNumber;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public void setIdNumber(String idNumber) {
        if (idNumber.length() == 13) {
           this.idNumber = idNumber;
        } else {
            throw new IllegalArgumentException("Please enter 13 Characters");
        }
        
    }

    //Setters
    public void setTitle(String title) {
        if (title.length() >=3 || title.length() >=2) {
            this.title = title; 
        } else {
            throw new IllegalArgumentException("Please enter not more than 3 characters or less than 2 characters");
        }
       
    }

    public void setName(String name) {
        if (name.length() > 3) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Please enter More than 3 Characters");
        }
        
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    //Display
    public String displayclientDetails(){
        return "-------------Client Details----------\n" 
                + "ID Number: " + idNumber + "\n"
                + "Title: " + title +"\n"
                + "Name: " + name + "\n"
                + address.toString();
    }

    //Display
    @Override
    public String toString() {
        return "-------------Client Details----------\n" 
                + "ID Number: " + idNumber + "\n"
                + "Title: " + title +"\n"
                + "Name: " + name ; 
                
    }

}
